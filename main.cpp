// Software Design M.EEC FEUP
// Main contributers: Ricardo Barbosa Sousa and Armando Sousa
// GPL v3
// In short, free software: Useful, transparent, no warranty

#include <iostream>
#include <vector>

// Comment the line below to use CPP vector instead of C StaticMaze
#define STATICMAZE

#ifdef STATICMAZE
    #define MAZESIZE 10

    /// Prints the maze (static C matrix) on the screen
    /// MAZESIZE is a global #define (constant value)
    void printMaze(const char theMazeToPrint[MAZESIZE][MAZESIZE]) // Turns into ptr to start of maze matrix
    {
      for (int i = 0; i < MAZESIZE; i++) {
        for (int j = 0; j < MAZESIZE; j++) {
          std::cout << theMazeToPrint[i][j];
        }
        std::cout << std::endl;
      }
    }
#endif  


/// Prints the maze (CPP vector of vector) on the screen
/// Class std::vector includes size() of the array
void printMaze(std::vector<std::vector<char>> &theMazeToPrint) 
{
  for (unsigned i = 0; i < theMazeToPrint.size(); i++) {
    // - print the maze coordinate (i,j)
    for (unsigned j = 0; j < theMazeToPrint[i].size(); j++) {
      std::cout << theMazeToPrint[i][j];
    }
    std::cout << std::endl;
  }
}

// Attention:
//    Go to line 10 (of this file) to select maze data structure 
//    as either as a    CPP vector    or    C static matrix

int main(int argc, char** argv) 
{
  // Local maze variable is inside a #if (preprocessor directive)
  #ifdef STATICMAZE
    // Maze as a static C biDimentional ("square") array, 0-based, of size MAZESIZE
    char maze[MAZESIZE][MAZESIZE] = {
        {'X','X','X','X','X','X','X','X','X','X'},
        {'X','H',' ',' ',' ',' ',' ',' ',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X','D','X','X',' ','X',' ','X',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X',' ',' ',' ',' ',' ',' ','X',' ','E'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X','K','X','X',' ',' ',' ',' ',' ','X'},
        {'X','X','X','X','X','X','X','X','X','X'}
    }; 
  #else
    // Store the maze as a CPP vector (CPP dynamic vector in this case of type <char>)
    std::vector<std::vector<char>> maze{
        {'X','X','X','X','X','X','X','X','X','X'},
        {'X','H',' ',' ',' ',' ',' ',' ',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X','D','X','X',' ','X',' ','X',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X',' ',' ',' ',' ',' ',' ','X',' ','E'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X',' ','X','X',' ','X',' ','X',' ','X'},
        {'X','K','X','X',' ',' ',' ',' ',' ','X'},
        {'X','X','X','X','X','X','X','X','X','X'}
    }; 
  #endif

  // Game loop
  int hx = 1, hy = 1;
  char uc;
  bool has_key = false, gameOver = false;
  
    do {
      printMaze(maze); // Attention: Two different functions at stake here, according to #define above!!!
                       // The two functions are said to be OVERLOADED one on top of the other!

      // - read user command
      std::cout << "cmd> ";
      std::cin >> uc;

      // - ignore the other characters
      std::cin.clear();                 // Clear error
      std::cin.ignore(INT_MAX, '\n');   // Ignore rest of input (trash it!)

      // - process command; note that maze[...][...] may refer to a static C vector or a C++ vector
      switch (uc) {
        case 'w': // UP
          if (maze[hx-1][hy] == ' ') {  // check for free space
            maze[hx-1][hy] = 'H';
            maze[hx][hy] = ' ';
            hx--;

            // check dragon
            if (maze[hx-1][hy] == 'D') {
              printMaze(maze);
              std::cout << "YOU DIED!" << std::endl;
              gameOver = true;
            }
          }
          break;

        case 's': // DOWN
          if (maze[hx+1][hy] == ' ') {  // check for free space
            maze[hx+1][hy] = 'H';
            maze[hx][hy] = ' ';
            hx++;

            // check dragon
            if (maze[hx+1][hy] == 'D') {
              printMaze(maze);
              std::cout << "YOU DIED!" << std::endl;
              gameOver = true;
            }

            break;
          }

          if (maze[hx+1][hy] == 'K') {  // check for the key
            maze[hx+1][hy] = 'H';
            maze[hx][hy] = ' ';
            hx++;
            has_key = true;
            std::cout << "You now have the KEY!" << std::endl;
          }
          break;

        case 'd': // RIGHT
          if (maze[hx][hy+1] == ' ') {  // check for free space
            maze[hx][hy+1] = 'H';
            maze[hx][hy] = ' ';
            hy++;
          }

          if (maze[hx][hy+1] == 'E') {  // check EXIT
            if (has_key) {
              std::cout << "GREAT! YOU REACHED THE EXIT!" << std::endl;
              gameOver = true;

            } else {
              std::cout << "You need a key to open this door." << std::endl;
            }
          }
          break;

        case 'a': // LEFT
          if (maze[hx][hy-1] == ' ') {  // check for free space
            maze[hx][hy-1] = 'H';
            maze[hx][hy] = ' ';
            hy--;
          }
          break;

        default:
          break;
      } // end of large switch case

    } while ((!gameOver) && (uc != 'q'));

}




